"""
Definition of urls for Agrupamento_N4_CP_V2.
"""
#!/usr/bin/python
# -*- coding: utf-8 -*-

from datetime import datetime
from django.conf.urls import url
import django.contrib.auth.views

import app.forms
import app.views

# Uncomment the next lines to enable the admin:
from django.conf.urls import include
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # Examples:
    url(r'^$', app.views.home, name='home'),
    url(r'^about$', app.views.about, name='about'),
    url(r'^lobitos$', app.views.lobitos, name='lobitos'),
    url(r'^exploradores$', app.views.exploradores, name='exploradores'),
    url(r'^pioneiros$', app.views.pioneiros, name='pioneiros'),
    url(r'^caminheiros$', app.views.caminheiros, name='caminheiros'),
    url(r'^dirigentes$', app.views.dirigentes, name='dirigentes'),
    url(r'^pesquisaBando$', app.views.pesquisaBando, name='pesquisaBando'),
    url(r'^findEscuteiro$', app.views.findEscuteiro, name='findEscuteiro'),
    url(r'^login/$',
        django.contrib.auth.views.login,
        {
            'template_name': 'app/login.html',
            'authentication_form': app.forms.BootstrapAuthenticationForm,
            'extra_context':
            {
                'title': 'Log in',
                'year': datetime.now().year,
            }
        },
        name='login'),
    url(r'^logout$',
        django.contrib.auth.views.logout,
        {
            'next_page': '/',
        },
        name='logout'),
    url(r'^inserirEscuteiro$', app.views.inserirEscuteiro, name='inserirEscuteiro'),
    url(r'^inserirLobitos$', app.views.inserirLobitos, name='inserirLobitos'),
    url(r'^inserirComentario$', app.views.inserirComentario, name='inserirComentario'),
    url(r'^registoUtilizador$', app.views.registoUtilizador, name='registoUtilizador'),
    url(r'^criarCargo$', app.views.criarCargo, name="criarCargo"),
    url(r'^criarBando$', app.views.criarBando, name="criarBando"),
    url(r'^getLobitos$', app.views.getLobitos, name="lobitos"),
    url(r'^getExploradores$', app.views.getExploradores, name="exploradores"),
    url(r'^getPioneiros$', app.views.getPioneiros, name="pioneiros"),
    url(r'^getCaminheiros$', app.views.getCaminheiros, name="caminheiros"),
    url(r'^getDirigentes$', app.views.getDirigentes, name="dirigentes"),
    url(r'^getEscuteiros$', app.views.getEscuteiros, name="escuteiros"),
    url(r'^getCargos$', app.views.getCargos, name="getCargos"),
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
]
