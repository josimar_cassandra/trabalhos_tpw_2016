# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Agrupamento',
            fields=[
                ('nome', models.CharField(primary_key=True, serialize=False, max_length=70)),
                ('endereco', models.CharField(max_length=70)),
                ('email', models.EmailField(max_length=254)),
                ('data_fundacao', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Bando',
            fields=[
                ('nome', models.CharField(primary_key=True, serialize=False, max_length=70)),
                ('elementos', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Caminheiro',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('data_promessa', models.DateField()),
                ('padroeiro', models.CharField(max_length=70)),
            ],
        ),
        migrations.CreateModel(
            name='Cargo',
            fields=[
                ('nome', models.CharField(primary_key=True, serialize=False, max_length=70)),
            ],
        ),
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('nome', models.CharField(max_length=50)),
                ('e_mail', models.EmailField(max_length=254)),
                ('coment', models.TextField()),
                ('dateTime', models.DateTimeField(primary_key=True, auto_now_add=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Cota',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('valor', models.IntegerField()),
                ('ano', models.CharField(max_length=7)),
                ('dataLimite', models.DateField()),
                ('dataPagamento', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Dirigente',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('data_promessa', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Escuteiro',
            fields=[
                ('nome', models.CharField(max_length=70)),
                ('sobrenome', models.CharField(max_length=70)),
                ('email', models.EmailField(max_length=254)),
                ('telefone', models.CharField(max_length=15)),
                ('data_nasc', models.DateField()),
                ('bi', models.CharField(primary_key=True, serialize=False, max_length=15)),
                ('foto', models.FileField(upload_to='C:/Users/Josi/Desktop/Agrupamento_N4_CP_V2/Agrupamento_N4_CP_V2/app/static/app/ImagensUpload/%Y/%m/%d/', max_length=400)),
            ],
        ),
        migrations.CreateModel(
            name='Explorador',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('data_promessa', models.DateField()),
                ('escuteiro', models.OneToOneField(to='app.Escuteiro')),
            ],
        ),
        migrations.CreateModel(
            name='Funcoes_Dirigente',
            fields=[
                ('nome', models.CharField(primary_key=True, serialize=False, max_length=70)),
            ],
        ),
        migrations.CreateModel(
            name='Lobito',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('data_promessa', models.DateField()),
                ('escuteiro', models.OneToOneField(to='app.Escuteiro')),
            ],
        ),
        migrations.CreateModel(
            name='Patrulha_Explorador',
            fields=[
                ('nome', models.CharField(primary_key=True, serialize=False, max_length=70)),
                ('elementos', models.IntegerField()),
                ('cargos', models.ManyToManyField(to='app.Cargo')),
                ('exploradores', models.ManyToManyField(to='app.Explorador')),
            ],
        ),
        migrations.CreateModel(
            name='Patrulha_Pioneiro',
            fields=[
                ('nome', models.CharField(primary_key=True, serialize=False, max_length=70)),
                ('elementos', models.IntegerField()),
                ('cargos', models.ManyToManyField(to='app.Cargo')),
            ],
        ),
        migrations.CreateModel(
            name='Pioneiro',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('data_promessa', models.DateField()),
                ('escuteiro', models.OneToOneField(to='app.Escuteiro')),
            ],
        ),
        migrations.CreateModel(
            name='Situacao',
            fields=[
                ('nome', models.CharField(primary_key=True, serialize=False, max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='Utilizador',
            fields=[
                ('username', models.CharField(primary_key=True, serialize=False, max_length=50)),
                ('password', models.CharField(max_length=20)),
                ('e_mail', models.EmailField(max_length=254)),
                ('seccao', models.CharField(max_length=20)),
            ],
        ),
        migrations.AddField(
            model_name='patrulha_pioneiro',
            name='pioneiros',
            field=models.ManyToManyField(to='app.Pioneiro'),
        ),
        migrations.AddField(
            model_name='dirigente',
            name='escuteiro',
            field=models.OneToOneField(to='app.Escuteiro'),
        ),
        migrations.AddField(
            model_name='dirigente',
            name='funcao',
            field=models.ForeignKey(to='app.Funcoes_Dirigente'),
        ),
        migrations.AddField(
            model_name='cota',
            name='escuteiro',
            field=models.ForeignKey(to='app.Escuteiro'),
        ),
        migrations.AddField(
            model_name='cota',
            name='situacao',
            field=models.ForeignKey(to='app.Situacao'),
        ),
        migrations.AddField(
            model_name='cargo',
            name='escuteiro',
            field=models.OneToOneField(to='app.Escuteiro'),
        ),
        migrations.AddField(
            model_name='caminheiro',
            name='escuteiro',
            field=models.OneToOneField(to='app.Escuteiro'),
        ),
        migrations.AddField(
            model_name='bando',
            name='cargos',
            field=models.ManyToManyField(to='app.Cargo'),
        ),
        migrations.AddField(
            model_name='bando',
            name='lobitos',
            field=models.ManyToManyField(to='app.Lobito'),
        ),
        migrations.AddField(
            model_name='agrupamento',
            name='escuteiros',
            field=models.ManyToManyField(to='app.Escuteiro'),
        ),
    ]
