#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.contrib import admin
from app.models import Agrupamento, Escuteiro, Lobito, Bando, Cargo, Explorador, Patrulha_Explorador, Pioneiro, Patrulha_Pioneiro, Caminheiro, Funcoes_Dirigente, Dirigente, Utilizador, Comentario, Situacao, Cota

# Registo dos modelos
admin.site.register(Agrupamento)
admin.site.register(Escuteiro)
admin.site.register(Lobito)
admin.site.register(Cargo)
admin.site.register(Bando)
admin.site.register(Explorador)
admin.site.register(Patrulha_Explorador)
admin.site.register(Pioneiro)
admin.site.register(Patrulha_Pioneiro)
admin.site.register(Caminheiro)
admin.site.register(Funcoes_Dirigente)
admin.site.register(Dirigente)
admin.site.register(Utilizador)
admin.site.register(Comentario)
admin.site.register(Situacao)
admin.site.register(Cota)
#admin.site.register(CargoLobitos)