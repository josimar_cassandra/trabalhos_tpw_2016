"""
Definition of models.
"""
#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models
from django.utils import timezone
import datetime
# Create your models here.
class Escuteiro(models.Model):
    nome = models.CharField(max_length=70)
    sobrenome = models.CharField(max_length=70)
    email = models.EmailField()
    telefone = models.CharField(max_length=15)
    data_nasc = models.DateField()
    bi = models.CharField(max_length=15, primary_key=True)
    #Para fazer upload de fotos para a nossa aplicação
    foto = models.FileField(upload_to='C:/Users/Josi/Desktop/Agrupamento_N4_CP_V2/Agrupamento_N4_CP_V2/app/static/app/ImagensUpload/%Y/%m/%d/', max_length=400)
    
    f_path = models.CharField(max_length=100)
    #f_path = foto.get_directory_name   "../../static/app/Imagens/Upload/"
    f_path = foto.get_directory_name
    def __str__(self):
        return self.nome+" "+self.sobrenome
 
class Cargo(models.Model):
    nome = models.CharField(max_length=70, primary_key=True)
    escuteiro = models.OneToOneField(Escuteiro) # Um cargo pertence a um Escuteiro
    def __str__(self):
        return self.nome 

class Agrupamento(models.Model):
    nome = models.CharField(max_length=70, primary_key=True)
    endereco = models.CharField(max_length=70)
    email = models.EmailField()
    data_fundacao = models.DateField()
    escuteiros = models.ManyToManyField(Escuteiro, blank=False) # Um agrupamento tem 1 ou muitos escuteiros
    def __str__(self):
        return self.nome

class Lobito(models.Model):
    escuteiro = models.OneToOneField(Escuteiro)# Um lobito é um escuteiro
    data_promessa = models.DateField() 
    def __str__(self):
        return self.escuteiro.nome+" "+self.escuteiro.sobrenome
    def __unicode__(self):
        return self.escuteiro 

#class CargoLobitos(models.Model):
#    nome = models.CharField(max_length=70, primary_key=True)
#    lobito = models.OneToOneField(Lobito) # Um cargo pertence a um Lobito
#    def __str__(self):
#        return self.nome

class Bando(models.Model):
    nome = models.CharField(max_length=70, primary_key=True)
    elementos = models.IntegerField()
    lobitos = models.ManyToManyField(Lobito) # Um bando tem 1 ou muitos Lobitos
    cargos = models.ManyToManyField(Cargo) # Um bando tem 1 ou muitos muitos cargos
    def __str__(self):
        return self.nome

class Explorador(models.Model):
    escuteiro = models.OneToOneField(Escuteiro)# Um explorador é um escuteiro
    data_promessa = models.DateField() 
    def __str__(self):
        return self.escuteiro.nome+" "+self.escuteiro.sobrenome
 

class Patrulha_Explorador(models.Model):
    nome = models.CharField(max_length=70, primary_key=True)
    elementos = models.IntegerField()
    exploradores = models.ManyToManyField(Explorador) # Uma Patrulha de Exploradores tem 1 ou muitos Exploradores
    cargos = models.ManyToManyField(Cargo) # Um Patrulha de Exploradores tem 1 ou muitos cargos
    def __str__(self):
        return self.nome

class Pioneiro(models.Model):
    escuteiro = models.OneToOneField(Escuteiro)# Um Pioneiro é um escuteiro
    data_promessa = models.DateField()
    def __str__(self):
        return self.escuteiro.nome+" "+self.escuteiro.sobrenome

class Patrulha_Pioneiro(models.Model):
    nome = models.CharField(max_length=70, primary_key=True)
    elementos = models.IntegerField()
    pioneiros = models.ManyToManyField(Pioneiro) # Uma Patrulha de Pioneiros tem 1 ou muitos Pioneiros
    cargos = models.ManyToManyField(Cargo) # Um Patrulha de Pioneiros tem 1 ou muitos cargos
    def __str__(self):
        return self.nome

class Caminheiro(models.Model):
    escuteiro = models.OneToOneField(Escuteiro)# Um Caminheiro é um escuteiro
    data_promessa = models.DateField()
    padroeiro = models.CharField(max_length=70)
    def __str__(self):
        return self.escuteiro.nome+" "+self.escuteiro.sobrenome

class Funcoes_Dirigente(models.Model):
    nome = models.CharField(max_length=70, primary_key=True)
    def __str__(self):
        return self.nome

class Dirigente(models.Model):
    escuteiro = models.OneToOneField(Escuteiro)# Um Dirigente é um escuteiro
    data_promessa = models.DateField()
    funcao = models.ForeignKey(Funcoes_Dirigente) # Um Dirigente tem uma Função no agrupamento (Esta função pode ser atribuida a um outro dirigente)
    def __str__(self):
        return self.escuteiro.nome+" "+self.escuteiro.sobrenome

class Utilizador(models.Model):
    username = models.CharField(max_length=50, primary_key=True)
    password = models.CharField(max_length=20)
    e_mail = models.EmailField()
    seccao = models.CharField(max_length=20)
    def __str__(self):
        return self.username


class Comentario(models.Model):
    nome = models.CharField(max_length=50)
    e_mail = models.EmailField(max_length=254)
    coment = models.TextField()
    dateTime = models.DateTimeField(auto_now_add=True, primary_key=True)
    def __str__(self):
        return self.nome+" "+str(self.dateTime)

class Situacao(models.Model):
    nome = models.CharField(max_length=10, primary_key=True)
    def __str__(self):
        return self.nome

class Cota(models.Model):
    valor = models.IntegerField()
    ano = models.CharField(max_length=7)
    dataLimite = models.DateField()
    dataPagamento = models.DateField()
    situacao = models.ForeignKey(Situacao)
    escuteiro = models.ForeignKey(Escuteiro)
    def __str__(self):
        return self.escuteiro.nome+ " : " + str(self.situacao) 