"""
Definition of views.
"""
#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.shortcuts import redirect
from django.conf import settings
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext
from datetime import datetime
from app.models import *
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Home',
        }
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        {
            'title':'Quem somos',
            'message':'Somos alunos do 3º Ano de Licenciatura em Engenharia Informática da Universidade de Aveiro, este trabalho foi realizado no âmbito da disciplina de Tecnologias e Programação Web.',        
        }
    )

def lobitos(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/lobito.html',
        {
            'title':'Lobitos',
            'message':'Iª Secção',
        }
    )

def exploradores(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/explorador.html',
        {
            'title':'Exploradores',
            'message':'IIª Secção',
        }
    )

def pioneiros(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/pioneiro.html',
        {
            'title':'Pioneiros',
            'message':'IIIª Secção',
        }
    )

def caminheiros(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/caminheiro.html',
        {
            'title':'Caminheiros',
            'message':'IVª Secção',
    
        }
    )

def dirigentes(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/dirigente.html',
        {
            'title':'Dirigentes',
            'message':'',

        }
    )
@csrf_exempt
def findEscuteiro(request):
    if 'biScout' in request.POST:
        biScout = request.POST['biScout']
        if biScout:
            request.session["pesquisa"] = biScout
            escut = Escuteiro.objects.get(bi=biScout)
            return render(request, 'app/pesquisaEscuteiro.html', {'scouts':escut, 'query':biScout})
        else:
            return render(request, 'app/pesquisaEscuteiro_form.html', {'error':True})
    else:
        return render(request, 'app/pesquisaEscuteiro_form.html', {'error':False})


def pesquisaBando(request):
    if 'pesqband' in request.POST:
        q = request.POST['pesqband']
        if q:
            request.session["pesquisa"] = q
            bando = Bando.objects.filter(nome__icontains=q)
            return render(request, 'app/pesquisaBando.html', {'bands':bando, 'query':q})
        else:
            return render(request, 'app/pesquisaBando_form.html', {'error':True})
    else:
        return render(request, 'app/pesquisaBando_form.html', {'error':False})

@csrf_exempt
def alterarEscuteiro(request):
    print(request.POST)
    if 'biScout' in request.POST:
        biScout = request.POST['request'] 
        if biScout:
            request.sesion["pesquisa"] = biScouts
            scout = Escuteiro.objects.filter(nome__icontains=biScout)
            print(scout)
            return render(request, 'app/index.html', {'escuteiro':scout, 'query':biScout})
        else:
            return render(request, 'app/index.html', {'error':True})
    else:
        return render(request, 'app/index.html', {'error':False})

@csrf_exempt
def registoUtilizador(request): 
    if 'userConta' in request.POST and 'passwordConta' in request.POST and 'optradio' in request.POST:
        userName = request.POST['userConta']
        password = request.POST['passwordConta']
        seccao = request.POST['optradio']
        if userName and password and seccao:
            usr = Utilizador(userName, password, seccao)
            usr.save()
            return render(request, 'app/index.html', {'error':False})
        else:
            return render(request, 'app/registoUtilizadorErro.html', {'error':True})
    else:
        return render(request, 'app/registoUtilizadorErro.html', {'error':False})

@csrf_exempt
def inserirEscuteiro(request): 
    if 'nome' in request.POST and 'sobreNome' in request.POST and 'email' in request.POST and 'telefone' in request.POST and 'dataNasc' in request.POST and 'bi' in request.POST and 'imagem_escuteiro' in request.POST:
        nome = request.POST['nome']
        sobreNome = request.POST['sobreNome']
        email = request.POST['email']
        telefone = request.POST['telefone']
        dataNasc = request.POST['dataNasc']
        biRegisto = request.POST['bi']
        imagem_escuteiro = request.POST['imagem_escuteiro']
        if nome and sobreNome and email and telefone and dataNasc and biRegisto and imagem_escuteiro:
            escut = Escuteiro(nome, sobreNome, email, telefone, dataNasc, biRegisto, imagem_escuteiro)
            escut.save()
            return render(request, 'app/index.html', {'error':False})
        else:
            return render(request, 'app/erroInsertEscut.html', {'error':True})
    else:
        return render(request, 'app/erroInsertEscut.html', {'error':False})
 
@csrf_exempt
def removerEscuteiro(request): 
    if 'bi' in request.POST:
        biRegisto = request.POST['bi']
        if biRegisto:
            Escuteiro.objects.get(biRegisto='123').delete()
            return render(request, 'app/index.html', {'error':False})
        else:
            return render(request, 'app/erroInsertEscut.html', {'error':True})
    else:
        return render(request, 'app/erroInsertEscut.html', {'error':False})

@csrf_exempt
def inserirComentario(request): 
    if 'nomeContactar' in request.POST and 'emailContactar' in request.POST and 'comentarioContactar' in request.POST:
        nome = request.POST['nomeContactar']
        e_mail = request.POST['emailContactar']
        coment = request.POST['comentarioContactar']
        if nome and e_mail and coment:
            comm = Comentario(nome, e_mail, coment,None)
            comm.save()
            return render(request, 'app/index.html', {'error':False})
        else:
            return render(request, 'app/erroInsertComentario.html',{'error':True})
    else:
        return render(request, 'app/erroInsertComentario.html', {'error':False})

# Devolve todos os escuteiros registados naquele agrupamento na base de dados
def getEscuteiros(request):
    #agrup = Agrupamento.objects.all()
    scout = Escuteiro.objects.all()
    cotas = Cota.objects.all()
    lob = Lobito.objects.all()
    exp = Explorador.objects.all()
    pion = Pioneiro.objects.all()
    camin = Caminheiro.objects.all()
    dirig = Dirigente.objects.all()
    return render(request, 'app/index.html', {'getEscuteiros': scout, 'getCotas':cotas, 'getLobitos':lob, 'getExploradores':exp, 'getPioneiros':pion, 'getCaminheiros':camin, 'getDirigentes':dirig})


# Devolve todos os lobitos registados na base de dados
def getLobitos(request):
    lob = Lobito.objects.all()
    scout = Escuteiro.objects.all()
    carg = Cargo.objects.all()
    return render(request, 'app/lobito.html', {'getLobitos': lob, 'getEscuteiros': scout, 'getCargos':carg})

# Devolve todos os cargos registados na base de dados
def getCargos(request):
    carg = Cargo.objects.all()
    return carg

# Devolve todos os exploradores registados na base de dados
def getExploradores(request):
    exp = Explorador.objects.all()
    scout = Escuteiro.objects.all()
    return render(request, 'app/explorador.html', {'getExploradores': exp, 'getEscuteiros': scout})

# Devolve todos os pioneiros registados na base de dados
def getPioneiros(request):
    pio = Pioneiro.objects.all()
    scout = Escuteiro.objects.all()
    return render(request, 'app/pioneiro.html', {'getPioneiros': pio, 'getEscuteiros': scout})

# Devolve todos os caminheiros registados na base de dados
def getCaminheiros(request):
    cam = Caminheiro.objects.all()
    scout = Escuteiro.objects.all()
    return render(request, 'app/caminheiro.html', {'getCaminheiros': cam, 'getEscuteiros': scout})

# Devolve todos os dirigentes registados na base de dados
def getDirigentes(request):
    dir = Dirigente.objects.all()
    scout = Escuteiro.objects.all()
    return render(request, 'app/dirigente.html', {'getDirigentes': dir, 'getEscuteiros': scout})

@csrf_exempt
def inserirLobitos(request): 
    if 'lobEscut' in request.POST and 'dataPromessa_Lob' in request.POST:
        lobEscut = request.POST['lobEscut']
        dataPromessa_Lob = request.POST['dataPromessa_Lob']
        
        print("\nEscut:",(lobEscut),"\nData_Prom:",dataPromessa_Lob,"\n")
        if lobEscut and dataPromessa_Lob:
            lob = Lobito(lobEscut, dataPromessa_Lob)
            lob.save()
            return render(request, 'app/lobito.html', {'error':False})
        else:
            return render(request, 'app/erroInsertEscuteiro.html',{'error':True})
    else:
        return render(request, 'app/erroInsertEscuteiro.html', {'error':False})

@csrf_exempt
def criarCargo(request): 
    lobitos = Lobito.objects.all()
    nomeslobitos = list()
    for l in lobitos:
        nomeslobitos.append(l.escuteiro)
    
    if 'nomeCargo' in request.POST:
        nome = request.POST['nomeCargo']
        scout = request.POST['id_LobCargo']
        #for lob in nomeslobitos:
        #    print("Lob: ",lob, "Scout: ",scout)

        if nome and scout:  
            ca = Cargo(nome, scout)
            ca.save()
            return render(request, 'app/lobito.html', {'error':False})
        else:
            return render(request, 'app/criarCargos.html',{'error':True})
    else:
        return render(request, 'app/criarCargos.html', {'error':False, 'nomes':nomeslobitos})

@csrf_exempt
def criarBando(request): 
    lobitos = Lobito.objects.all()
    nomeslobitos = list()
    for l in lobitos:
        nomeslobitos.append(l.escuteiro)

    if 'nomeBando' in request.POST and 'numElemBando' in request.POST and 'lobitos' in request.POST and 'cargosLob' in request.POST:
        nomeBando = request.POST['nomeBando']
        numElemBando = request.POST['numElemBando']
        lobitos = request.POST.getlist('lobitos')
        cargosLob = request.POST.getlist('cargosLob')
        lobos = list()
        cargos = list()
        for l in lobitos:
            esc = Escuteiro.objects.get(bi=l)
            lobos.append(esc)
        for c in cargosLob:
            cargos.append(c)
        #print(nomeBando,"\n",numElemBando,"\n",lobos,"\n",cargos)
        #return HttpResponse("Inseriu Bando")
        if nomeBando and numElemBando and lobos and cargos:
            #print(nomeBando,"\n",numElemBando,"\n",lobos,"\n",cargos)  
            bando = Bando(nomeBando, numElemBando, lobos, cargos)
            bando.save()
            return render(request, 'app/lobito.html', {'error':False})
        else:
            return render(request, 'app/criarBandos.html',{'error':True})
    else:
        return render(request, 'app/criarBandos.html', {'error':False, 'nomes':nomeslobitos})

