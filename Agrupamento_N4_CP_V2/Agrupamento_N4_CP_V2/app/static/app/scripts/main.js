/**
 * Created by Josi on 24/03/2016.
 */

$(main);

function main(){
    $("#validarRegistoEscuteiro").click(function () { validarRegistoEscuteiro() });
    $("#criacaoConta").click(function () { criacaoConta() });
    $("#verificarPassWord").click(function() {verificarPassWord()});
    $("#recuperarPassword").click(function () { recuperarPassword() });
    $("#validacaoLogin").click(function () { validacaoLogin() });
    $("#login").click(function(){login()});
    $("#validMail").click(function(){validMail()});
    $("#logout").click(function () { logout() });
    $("#inscricao").click(function() {inscricao()});
    $("#enviarComentario").click(function() {enviarComentario()});
    $("#activarEdicao").click(function() {activarEdicao()});
    $("#desactivarEdicao").click(function() {desactivarEdicao()});
    $("#leituraNovaFoto").click(function () { leituraNovaFoto() });
    $("#inscreverLobito").click(function () { inscreverLobito() });
}

function inscreverLobito() {
    alert("inscreverLobito")
    //$("#inscLobBT").click(function () {
    //    $("inscrExp").hide();
    //});
    
   document.getElementById("inscrExp").style.display = 'none';
   
}


function criacaoCargo() {
    if (document.getElementById('nomeCargo').value !="") {
        alert("Cargo criado com sucesso!")
    }
}

function criacaoConta() {
    var user = document.getElementById('userConta').value;
    var pass = document.getElementById('passwordConta').value;
    var confpass = document.getElementById('confPassConta').value;
    var emailUtil = document.getElementById('emailUtil').value;
    // Para obter o valôr que foi selecionado
    var seccao = document.getElementsByName('scout');
    var lobito = document.getElementById('lobConta');
    var explorador = document.getElementById('expConta');
    var pioneiro = document.getElementById('pioConta');
    var caminheiro = document.getElementById('camConta');
    var dirigente = document.getElementById('dirConta');
    /**
     * Código para criar conta
     * **/
    if (!verificarPassWord()){
        alert("Password não coinscidem!");
    }
    
   
    else if (user != "" && pass != "" && confpass != "" && emailUtil != "" && verificarPassWord() && seccao != "") {
        if (lobito.checked) {
            seccao = document.getElementById('lobConta').value;
        } else if (explorador.checked) {
            seccao = document.getElementById('expConta').value;
        }
        else if (pioneiro.checked) {
            seccao = document.getElementById('pioConta').value;
        }
        else if (caminheiro.checked) {
            seccao = document.getElementById('camConta').value;
        } else if (dirigente.checked) {
            seccao = document.getElementById('dirConta').value;
        }
        alert("O seu registo foi efectuado com sucesso e encontra-se pendente para a validação.\nApós o processo da validação dos seus dados irá receber no e-mail '" + emailUtil + "' a confirmação da sua conta.");
    }
      
}

function verificarPassWord() {
   if (document.getElementById('passwordConta').value == document.getElementById('confPassConta').value) {
        return true;
    } else {
        return false;
    }
}

function recuperarPassword(){
    
    if (document.getElementById('user_rec').value != "" && validMail(document.getElementById('user_rec').value))
    {
        //alert("Username: " + document.getElementById('id_username').value);

        alert("Foi enviado para " + document.getElementById('user_rec').value + " a sua nova senha.");
    }
}

/**
* Método responsável por enviar comentários inseridos pelos utilizadores
**/
function enviarComentario() {
    if (document.getElementById('nomeContactar').value != "" 
            && document.getElementById('emailContactar').value != ""
            && document.getElementById('comentario').value != "")
    {
        alert("O seu comentário foi enviado com sucesso!");
    }
}

/**
 * Método responsável por fazer a válidação do Login com base no ficheiro json
 * **/
function validacaoLogin(){

    // utilizador introduzido
    var username = document.getElementById("utilizador").value;
    // password introduzido
    var password = document.getElementById("password").value;

    var contador = 0;
    var users;
    $.ajax({
        url: 'Json_package/dirigentes.json',
        dataType: 'json',
        type: 'get',
        cache: false,
        success: function(data){

            $(data.dirigentes).each(function(index, value){
                contador++;
                if((username == value.Utilizador) && (password == value.Password)){
                    window.location = "Inicio.html"; // Redireciona a página Inicio.html.
                    return false;
                }
                if (contador==data.dirigentes.length){
                        alert("Utilizador/Senha está inválida!");
                }
            }
            );

        }
    });
}


function login(){
    if(!validMail(document.getElementById('utilizador').value))
        alert("Utilizador inválido!");
    else
    {
        if (document.getElementById('password').value.length < 1) {
            alert("Password vazia");
        }
        else {
            validacaoLogin();
        }
    }
}


/*Para ativar o bloqueio dos inputs*/
function activarEdicao() {
    document.getElementById("nomeEscut").disabled = false;
    document.getElementById("sobrenomeEscut").disabled = false;
    document.getElementById("emailEscut").disabled = false;
    document.getElementById("telefoneEscut").disabled = false;
    document.getElementById("dataNascim_Escut").disabled = false;
    document.getElementById("imagemUpload").disabled = false;
    document.getElementById("biEscut").disabled = false;
    document.getElementById("lobEscut").disabled = false;
    document.getElementById("expEscut").disabled = false;
    document.getElementById("pioEscut").disabled = false;
    document.getElementById("camEscut").disabled = false;
    document.getElementById("dirEscut").disabled = false;
}
/*Para desativar o bloqueio dos inputs*/
function desactivarEdicao() {
    document.getElementById("nomeEscut").disabled = true;
    document.getElementById("sobrenomeEscut").disabled = true;
    document.getElementById("emailEscut").disabled = true;
    document.getElementById("telefoneEscut").disabled = true;
    document.getElementById("dataNascim_Escut").disabled = true;
    document.getElementById("imagemUpload").disabled = true;
    document.getElementById("biEscut").disabled = true;
    document.getElementById("lobEscut").disabled = true;
    document.getElementById("expEscut").disabled = true;
    document.getElementById("pioEscut").disabled = true;
    document.getElementById("camEscut").disabled = true;
    document.getElementById("dirEscut").disabled = true;
}


/*Para alterar a foto do escuteiro*/
function leituraNovaFoto(input){
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#imagem_escuteiro_Escut')
                .attr('src', e.target.result)
                .width(250)
                .height(250);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function validarRegistoEscuteiro(){

    if(document.getElementById('inputNome').value.length < 1){
        alert("Nome vazio!");

    }
    else if (document.getElementById('inputSobrenome').value.length < 1) {
            alert("Sobrenome vazio!");
    }


    else if (document.getElementById('inputEmail').value.length < 1) {
        alert("E-mail vazio!");
    }

    else if(!validMail(document.getElementById('inputEmail').value)){
        alert("E-mail inválido");
    }
    else if (document.getElementById('inputDataN').value.length < 1) {
        alert("Data de Nascimento vazio!");
    }
}


function logout() {
    if (confirm('Tem a certeza que quer sair?')) {
        return true;
    } else {
        return false;
    }
}

function validMail(email){
    var tmp = email.split("@");
    if(tmp.length > 1)
        if(tmp[0].length > 0 && tmp[1].length > 0)
            if(tmp[1].split("\.").length>1)
                return true;
    return false;
}

// Para validar inscrição
function inscricao() {
    if(document.getElementById('biRegisto').value != "" 
        /*document.getElementById('sobreNome').value != "" &&
        document.getElementById('email').value != "" &&
        document.getElementById('telefone').value != "" &&
        document.getElementById('inputData').value != "" &&
        document.getElementById('biRegisto').value != "" */
        )
        alert("Inscrição efetuada com sucesso!");
    
}

//Para escrita de ficheiro
function testReadJsonFile(){
    //alert('testReadJsonFile');
    /*
    $.ajax({
        url: 'app/data/dirigentes.json',
        dataType: 'json',
        type: 'get',
        cache: false,
        success: function(data){
            alert('sucesso!!!');
            /*$(data.dirigentes).each(function(index, value){
                alert(value.Utilizador, value.Password);
            }
            );

        }
    });*/
    $.getJSON('data/dirigentes.json', function() {
        alert('success');
    });
}
